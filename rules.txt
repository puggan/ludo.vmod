1. init
1.1. 2-4 player
1.2. each player starts with 4 tokens at their yard
1.3. a roll for each player may be done to decide on which player goes first
2. start of turn
2.1. each turn the player should roll the die
3. move
3.1. if the die is a 6, the player may move a token from the yard to the start, no more moves are allowed for this roll, but you earned a reroll, (see 5)
3.2. if the player have a token in play (on game track) it may move one of them forward, same amount of steps as the die show.
3.3. forward
3.3.1. if the token have walked around the board, to the square next to the home of its own color, the direction forward are on to the home, 
3.3.2. if on the home squares forward is in to the middle until the finish is reached
3.3.3. if you reach the finish, and still have steps left to walk, that move is invalid.
3.3.4. for all other squares on the game track the direction forward is clockwise around the game track.
3.4. landing
3.4.1. if the landing square is occupied with your own token, that move is invalid, unless its the finish square.
3.4.2. if the landing square is occupied with someone elses token, that token are return to thiere yead.
4. winner?
4.1. if the player have moved all 4 tokens to the finish, the game is over, and that player won.
5. reroll
5.1. if the die is a 6, the player roll the die again
5.2. if the reroll is below 6 or this is the first rerolled 6 this turn, the player can do another move (see 3)
5.3. if the reroll is 6 and this is the 2nd reroll this turn (three 6 rolls in a row) the turn is over. (see 6)
6. turn over
6.1. the turn goes to the next player, clockwise (back to 2)

